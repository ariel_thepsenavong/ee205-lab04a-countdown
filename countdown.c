///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Ariel Thepsenavon <arielat@hawaii.edu>
// @date   2/4/21
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

int main(int argc, char* argv[]) {
   time_t current_time, before_time;
   double diff_t;
   struct tm ref_time;
      ref_time.tm_sec = 7;
      ref_time.tm_min = 26;
      ref_time.tm_hour = 4;
      ref_time.tm_mday = 21;
      ref_time.tm_mon = 0;
      ref_time.tm_year = 114; //years since 1900
      ref_time.tm_wday = 2;
      ref_time.tm_yday = 20;
      ref_time.tm_isdst = 0;
  
   //struct tm * timeinfo;
     // before_time = mktime(&ref_time);
   printf("Reference time: %s \n", asctime(&ref_time));
   while(1){
      time(&current_time);
      struct tm *info = localtime(&current_time);
      int sec_diff = fabs(info->tm_sec - ref_time.tm_sec +7);
      int min_diff = fabs(info->tm_min - ref_time.tm_min);
      int hours_diff = fabs(info->tm_hour - ref_time.tm_hour);
      int days_diff = fabs(info->tm_yday - ref_time.tm_yday);
      int years_diff = fabs(info->tm_year - ref_time.tm_year);
      
      printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d \n", years_diff, days_diff, hours_diff, min_diff, sec_diff);
      sleep(1);
   }
   return 0;
}
